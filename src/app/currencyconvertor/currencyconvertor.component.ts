import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-currencyconvertor',
  templateUrl: './currencyconvertor.component.html',
  styleUrls: ['./currencyconvertor.component.css']
})
export class CurrencyconvertorComponent implements OnInit {
  amount1: Number;
  countrycode;
  url;
  arr = {};
  convertamount;
  data;
  showmoney: boolean = false;
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  //curreny convertor 
  submit(form: NgForm) {
    this.amount1 = form.value.amount;
    this.countrycode = form.value.currencies;
    this.url = "https://data.fixer.io/api/convert?access_key=a22225565c9b3d18d2ebb3694ef5f6e4&from=INR&to=" + this.countrycode + "& amount=" + this.amount1;
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get(this.url, { headers }).subscribe((res) => {
      this.data = res;
      this.showmoney = true;
      this.convertamount = this.data.result;
      console.log(this.convertamount);
    });

  }
}

//this.showmoney = true;
//this.convertamount = res.result;
//console.log(this.convertamount);