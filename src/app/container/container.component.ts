import { Component, OnInit } from '@angular/core';
import { Shoe } from '../pdata';
import { GreendeckService } from '../greendeck.service';
import { Currency } from '../model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'


@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  public shoe_data: any;
  public code: String;
  public data;
  public url: string = "";
 
  constructor(private ser: GreendeckService, private http: HttpClient) {
    this.shoe_data = this.ser.get_data();
    // console.log(this.shoe_data[0].amount);
  }

  ngOnInit() {

  }

 
    
  }

