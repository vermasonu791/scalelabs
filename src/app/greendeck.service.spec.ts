import { TestBed } from '@angular/core/testing';

import { GreendeckService } from './greendeck.service';

describe('GreendeckService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GreendeckService = TestBed.get(GreendeckService);
    expect(service).toBeTruthy();
  });
});
