export const product =
    [
        {
            "brand_name": "Nike",
            "description":"runner shoes",
            "img": "../../assets/img/1.jpg",
            "amount":500
        },

        {
            "brand_name": "Nike",
            "description":"sneakers",
            "img": "../../assets/img/2.jpg",
            "amount":400
        },
        {
            "brand_name": "Puma",
            "description":"casual sneakers",
            "img": "../../assets/img/3.jpg",
            "amount":600
        },
        {
            "brand_name": "Puma",
            "description":"Black Studs",
            "img": "../../assets/img/4.jpg",
            "amount":800
        },
      
        {
            "brand_name": "Armani",
            "description":"Glasses",
            "img": "../../assets/img/9.jpg",
            "amount":1000
        },
        {
            "brand_name": "Ray-ban",
            "description":"Glasses",
            "img": "../../assets/img/10.jpg",
            "amount":300

        },
        {
            "brand_name": "Nike",
            "description":"T-shirt",
            "img": "../../assets/img/11.jpg",
            "amount":999
        },
        {
            "brand_name": "woodland",
            "description":"shirt",
            "img": "../../assets/img/12.jpg",
            "amount":890
        },
        {
            "brand_name": "Levis",
            "description":"shorts",
            "img": "../../assets/img/13.jpg",
            "amount":1100
        }
        ,
        {
            "brand_name": "woodland",
            "description":"shorts",
            "img": "../../assets/img/14.jpg",
            "amount":1000

        }
        ,
        {
            "brand_name": "Nike",
            "description":"Denim Jeans",
            "img": "../../assets/img/15.jpg",
            "amount":1199

        }
        ,
        {
            "brand_name": "Roadstar",
            "description":"jeans",
            "img": "../../assets/img/16.jpg",
            "amount":700

        },
        {
            "brand_name": "Puma",
            "description":"Canvas Sneakers",
            "img": "../../assets/img/5.jpg",
            "amount":650
        },

        {
            "brand_name": "Adidas",
            "description":"Blue Runner shoe",
            "img": "../../assets/img/6.jpg",
            "amount":550
        },
        {
            "brand_name": "Puma",
            "description":"sport shoes",
            "img": "../../assets/img/7.jpg",
            "amount":799

        },
        {
            "brand_name": "Nike",
            "description":"White Runner shoes",
            "img": "../../assets/img/8.jpg",
            "amount":699
        }
    ]
