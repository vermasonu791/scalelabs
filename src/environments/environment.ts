// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config:{
    apiKey: "AIzaSyDOSLmmimJOSbrat_51tTy0QuPG9PXjiMs",
    authDomain: "scalelabs-c73af.firebaseapp.com",
    databaseURL: "https://scalelabs-c73af.firebaseio.com",
    projectId: "scalelabs-c73af",
    storageBucket: "scalelabs-c73af.appspot.com",
    messagingSenderId: "25579728393"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
