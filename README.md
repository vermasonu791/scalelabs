# Greendeck

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.3.

I have done this project on angular 6 single page application framework.

##Technolgy used
1) Angular 6 
2) css

##Live web site link:-
1.Enable Cors origin resource sharing `https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en`
2.`https://scalelabs-c73af.firebaseapp.com/`

## currency convertor
I have created currency convertor on to top of page .
step-1 Enter the amount 
step-2 select country code from the drop down.

##Step to Run code
1) install node `https://nodejs.org/en/`

2) install angular `npm install -g @angular/cli`

3) Clone the project from bitbucket git clone 
`git clone git clone https://vermasonu791@bitbucket.org/vermasonu791/scalelabs.git`

4)After download go to directory and

5)And run command `npm install`

6)Run `ng serve` for a dev server. `Navigate to http://localhost:4200/`. The app will automatically reload if you change any of the source files.

###Thank you
